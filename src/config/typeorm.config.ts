import { TypeOrmModuleOptions } from "@nestjs/typeorm";

export const typeOrmConfig: TypeOrmModuleOptions = {
    type: 'mysql',
    host: '127.0.0.1',
    port: 3306,
    username: 'root',
    password: 'pepenn06',
    database: 'book_api',
    entities: [__dirname + '/../**/*.entity.{ts,js}'],
    synchronize: true
}