import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from "@nestjs/common";
import { isUUID } from "class-validator";

@Injectable()
export class UUIDValidator implements PipeTransform {
    transform(value: any) {
        if(!isUUID(value, 4)){
            throw new BadRequestException(`value ${value} is not UUID`)
        }
        return value
    }
}