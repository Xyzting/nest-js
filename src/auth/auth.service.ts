import { Injectable, InternalServerErrorException, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { refreshTokenConfig } from 'src/config/jwt.config';
import { User } from 'src/users/entity/user.entity';
import { UsersService } from 'src/users/users.service';
import { Repository } from 'typeorm';
import { LoginDto } from './dto/login.dto';
import { RefreshAccessTokenDto } from './dto/refersh.dto';
import { RefreshToken } from './entity/refresh-token.entity';
import { LoginResponse } from './interface/log-respon.interface';
import { TokenExpiredError } from 'jsonwebtoken';

@Injectable()
export class AuthService {
    constructor( 
        private readonly jwtService: JwtService,
        private readonly usersService: UsersService,
        @InjectRepository(RefreshToken)
        private readonly refreshTokenRepository: Repository<RefreshToken>
    ) {}

    async login(loginDto: LoginDto): Promise<LoginResponse> {
        const { email, password } = loginDto;

        const user = await this.usersService.validateUser(email, password)
        if(!user) {
            throw new UnauthorizedException("Email or Password Wrong")
        }

        const access_token = await this.createAccessToken(user)
        const refresh_token = await this.getRefreshToken(user)
        return { access_token, refresh_token } as LoginResponse
    }

    async refreshAccessToken(refreshTokenDto: RefreshAccessTokenDto): Promise<{access_token}> {
        const { refresh_token } = refreshTokenDto
        const payload = await this.decodeToken( refresh_token )
        const refreshToken = await this.refreshTokenRepository.findOne({ where: {id: payload.jid }, relations: ['user'] })
        if(!refreshToken){
            throw new UnauthorizedException('Refresh Token not found')
        }
        
        if(refreshToken.isRevoked) {
            throw new UnauthorizedException('Refresh Token has been revoked')
        }

        const access_token = await this.createAccessToken(refreshToken.user)

        return { access_token }
    }

    async decodeToken(token: string): Promise<any> {

        try {
            return await this.jwtService.verifyAsync(token)
        }catch(err) {
            if(err instanceof TokenExpiredError ){
                throw new UnauthorizedException("Refresh Token is Expired")
            }else {
                throw new InternalServerErrorException('Failed to decode token')
            }
        }
    }

    async createAccessToken(user: User): Promise<string> {
        const payload = {
            sub: user.id
        }
        const access_token = await this.jwtService.signAsync(payload)
        return access_token
    }

    async createRefreshToken(user: User, ttl: number): Promise<RefreshToken>{
        const refreshToken = this.refreshTokenRepository.create()
        refreshToken.user = user
        refreshToken.isRevoked = false
        const expiredAt = new Date()
        expiredAt.setTime(expiredAt.getTime() + ttl)
        refreshToken.expiredAt = expiredAt

        return await refreshToken.save()
    }

    async getRefreshToken(user: User): Promise<string> {
        const refreshToken = await this.createRefreshToken(
            user, 
            +refreshTokenConfig.expiresIn
        )
        const payload = {
            jid: refreshToken.id
        }
        const refresh_token = await this.jwtService.signAsync(payload, refreshTokenConfig)
        return refresh_token
    }

    async revokeRefreshToken(id: string): Promise<void>{
        const refreshToken = await this.refreshTokenRepository.findOne({where: {id: id}})
        if(!refreshToken) {
            throw new NotFoundException('Refresh token is not found')
        }
        refreshToken.isRevoked = true
        await refreshToken.save()
    }

}








                                                                                                                                                                                