import { Body, Controller, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { RefreshAccessTokenDto } from './dto/refersh.dto';
import { LoginResponse } from './interface/log-respon.interface';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Post()
    async login(@Body() loginDto: LoginDto): Promise<LoginResponse>{
        return this.authService.login(loginDto)
    }

    @Post('refresh-token')
    async refreshToken(@Body() refreshTokenDto: RefreshAccessTokenDto): Promise<{access_token: string}>{
        return this.authService.refreshAccessToken(refreshTokenDto)
    }

    @Patch('/:id/revoke')
    @UseGuards(AuthGuard('jwt'))
    async revokeRefreshToken(@Param('id') id: string): Promise<void>{
        return this.authService.revokeRefreshToken(id)
    }
}
