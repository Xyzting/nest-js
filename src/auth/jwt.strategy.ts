import { UnauthorizedException, Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy, ExtractJwt } from 'passport-jwt'
import { jwtConfig } from "src/config/jwt.config";
import { UsersService } from "src/users/users.service";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    constructor(private readonly userService: UsersService) {
        super ({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpirations: true,
            secretOrKey: jwtConfig.secret
        })
    }

    async validate(payload: any) {
        const user = await this.userService.findUser(payload.sub)
        if(!user) {
            throw new UnauthorizedException('User Not Found!')
        }

        return user
    }
}