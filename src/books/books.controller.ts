import { Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/auth/get-user.decorator';
import { UUIDValidator } from 'src/pipes/uuid-validator';
import { User } from 'src/users/entity/user.entity';
import { BooksService } from './books.service';
import { CreateDto } from './dto/create.dto';
import { FilterDto } from './dto/filter.dto';
import { UpdateDto } from './dto/update.dto';
import { Book } from './entity/book.entity';

@Controller('books')
@UseGuards(AuthGuard('jwt'))
export class BooksController {
    constructor(private booksService: BooksService) {}

    @Get()
    getBooks(
        @Query() filter: FilterDto, 
        @GetUser() user: User
    ): Promise<Book[]> {
        return this.booksService.getBooks(user, filter)
    }

    @Get('/:id')
    getBook(
        @GetUser() user: User, 
        @Param('id', UUIDValidator) id: string
    ): Promise<Book>{
        return this.booksService.getbook(user, id)
    }

    @Post()
    createBook(
        @Body() payload: CreateDto, 
        @GetUser() user: User
    ): Promise<void> {
        return this.booksService.createBook(user, payload)
    }

    @Put('/:id')
    updateBook(
        @GetUser() user: User,
        @Param('id') id: string, 
        @Body() payload: UpdateDto
    ): Promise<void> {
        return this.booksService.updateBook(user, id, payload)
    }   
    
    @Delete('/:id')
    deleteBook(
        @GetUser() user: User,
        @Param('id') id: string
    ): Promise<void>{
        return this.booksService.deleteBook(user, id)
    }
}
