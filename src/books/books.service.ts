import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entity/user.entity';
import { Repository } from 'typeorm';
import { CreateDto } from './dto/create.dto';
import { FilterDto } from './dto/filter.dto';
import { UpdateDto } from './dto/update.dto';
import { Book } from './entity/book.entity';



@Injectable()
export class BooksService {
    constructor(
        @InjectRepository(Book)
        private readonly book: Repository<Book>
    ) {}


    async getBooks(user: User, filter: FilterDto): Promise<Book[]>{
        const { title, author, genre, min_years, max_years } = filter
        const query = this.book.createQueryBuilder('book')
                        .where(`book.userId = :userId`, { userId: user.id } )
        if(title) {
            query.andWhere('lower(book.title)LIKE :title',{
                title: `%${title.toLowerCase()}%`
            })
        }
        if(author) {
            query.andWhere('lower(book.author) LIKE :author ', {
                author: `%${author.toLowerCase()}%`
            })
        }
        if(genre) {
            query.andWhere('lower(book.genre) LIKE :genre', {
                genre: `%${genre.toLowerCase()}%`
            })
        }
        if(min_years) {
            query.andWhere('book.years <= :min_years', {min_years})
        }
        if(max_years) {
            query.andWhere('book.years >= :max_years', {max_years})
        }

        return await query.getMany()
    }

    async createBook(user: User, createBook: CreateDto): Promise<void> {
        const {title, author, genre, years} = createBook
        const book = this.book.create()
        book.title = title
        book.author = author
        book.genre = genre
        book.years = years
        book.user = user

        try {
            await book.save()
        }catch(err){
            throw new InternalServerErrorException(err)
        }
    }

    async getbook(user: User, id: string): Promise<Book>{
        const book = await this.book.findOne({ where: { id: id, user: { id } }})
        if(!book){
            throw new NotFoundException(`Book with ${id} is not found`);
        }
        return book
    }

    async updateBook(user: User, id: string, updateDto: UpdateDto): Promise<void>{ 
        const {title, author, genre, years} = updateDto
        const book = await this.getbook(user, id)
        book.title = title
        book.author = author
        book.genre = genre 
        book.years = years

        try {
            await book.save()
        }catch(err) {
            throw new InternalServerErrorException(err)
        }
    }

    async deleteBook(user: User, id: string): Promise<void>{
        try {
            await this.book.delete({id: id, user: {id}})
        }catch(err) {
            throw new InternalServerErrorException(err)
        }
    }
}

