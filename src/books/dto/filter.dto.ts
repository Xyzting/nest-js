import { Type } from "class-transformer"
import { IsInt, IsOptional } from "class-validator"

export class FilterDto {

    @IsOptional()
    title: string

    @IsOptional()
    author: string

    @IsOptional()
    genre: string

    @IsOptional()
    @IsInt()
    @Type(() => Number)
    min_years: number

    @IsOptional()
    @IsInt()
    @Type(() => Number)
    max_years: number
}
