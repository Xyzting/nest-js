import { ConflictException, Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create.dto';
import { User } from './entity/user.entity';
import * as bcrypt from 'bcrypt'

@Injectable()
export class UsersService {
    constructor (
        @InjectRepository(User)
        private readonly user: Repository<User>
    ){}

    async createUser(createDto: CreateUserDto): Promise<void> {
        const { username, email, password } = createDto
        const user = this.user.create()
        user.username = username
        user.email = email
        user.salt = await bcrypt.genSalt();
        user.password = await bcrypt.hash(password, user.salt)

        try {
            await user.save()
        }catch(err){
            if(err.errno === 1062){
                throw new ConflictException(`Email ${email} is already used`)
            }else {
                throw new InternalServerErrorException(err)
            }
        }

    }

    async validateUser(email: string, password: string): Promise<User> {
        const user = await this.user.findOneBy({ email })

        if( user && (await user.validatePass(password)) ) {
            return user;
        }

        return null
    }

    async findUser(id: string): Promise<User>{
        return await this.user.findOne({ where: {id: id} })
    }
}
