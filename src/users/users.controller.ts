import { Body, Controller, Post } from '@nestjs/common';
import { CreateUserDto } from './dto/create.dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
    constructor(private readonly userService: UsersService) {}

    @Post()
    createUser(@Body() payload: CreateUserDto): Promise<void>{
        return this.userService.createUser(payload)
    }
}
